<?php

namespace App\services\TripSorter;

use App\services\TripSorter\interfaces\BoardingCardInterface;

class RouteWriter
{
    private $route = [];
    const FINAL_POINT = "You have arrived at your final destination." . PHP_EOL;

    /**
     * RouteWriter constructor.
     * @param BoardingCardInterface[] $route
     * @throws exceptions\WriterStringConvertException
     */
    public function __construct(array $route)
    {
        foreach ($route as $item) {
            if(!$this->canBeConvertedToString($item)) throw new exceptions\WriterStringConvertException;
        }
        $this->route = $route;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        $string = '';
        for($i=1;$i<=count($this->route);$i++) {
            $string .= "$i. {$this->route[$i-1]}" . PHP_EOL;
        }
        $string .= "$i. " . self::FINAL_POINT;
        return trim($string);
    }

    /**
     * Check if array item of $route array can be represented as string
     * @param $item
     * @return bool
     */
    private function canBeConvertedToString($item) : bool
    {
        return !is_array( $item ) &&
        ( 
            (!is_object( $item ) && settype( $item, 'string' ) !== false ) ||
            ( is_object( $item ) && method_exists( $item, '__toString' )) 
        );
    }


}