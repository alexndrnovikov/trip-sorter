<?php

namespace App\services\TripSorter;

use App\services\TripSorter\interfaces\BoardingCardInterface;

class TripSorter
{
    private /** @var BoardingCardInterface[] $boardingCards */ $boardingCards = [];
    private /** @var array $routeMap */ $routeMap = [];

    /**
     * TripSorter constructor.
     * @param BoardingCardInterface[] $boardingCards
     * @throws exceptions\MultipleDeparturePointsException | exceptions\ConstructException
     */
    public function __construct(array $boardingCards)
    {
        foreach ($boardingCards as $boardingCard) {
            if(!$boardingCard instanceof BoardingCardInterface) throw new exceptions\ConstructException();
            $this->boardingCards[$boardingCard->getDeparturePoint()] = $boardingCard;
            $this->routeMap[$boardingCard->getDeparturePoint()] = $boardingCard->getDestinationPoint();
        }
        if(count($boardingCards) != count($this->routeMap)) {
            throw new exceptions\MultipleDeparturePointsException();
        }
        return $this;
    }

    /**
     * @return BoardingCardInterface[]
     */
    public function getSortedRouteMap() : array
    {
        $this->sortBoardingCards();
        return $this->boardingCards;
    }

    /**
     * Sort $boardingCards array in real route order
     */
    private function sortBoardingCards() : void
    {
        /**
         * start node
         */
        $pointer = array_values(array_diff(array_keys($this->routeMap), array_values($this->routeMap)))[0];
        $sortedRoute = [];
        while (true) {
            if(!array_key_exists($pointer, $this->boardingCards)) break;
            $sortedRoute[] = $this->boardingCards[$pointer];
            $pointer = $this->boardingCards[$pointer]->getDestinationPoint();
        }
        $this->boardingCards = $sortedRoute;
    }
}