<?php

namespace App\services\TripSorter\interfaces;

interface BoardingCardInterface {

    /**
     * @return string
     */
    public function getDestinationPoint();

    /**
     * @return string
     */
    public function getDeparturePoint();

    /**
     * @return string
     */
    public function getSeat();

    /**
     * @return string
     */
    public function getTransportType();

    /**
     * @return string
     */
//    public function getTransportId();

    /**
     * Data to string conversation
     * @return string
     */
    public function __toString();
}