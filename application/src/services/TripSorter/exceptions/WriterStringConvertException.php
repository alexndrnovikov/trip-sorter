<?php

namespace App\services\TripSorter\exceptions;

/**
 * Variable can't be converted to string
 * Class WriterStringConvertException
 * @package App\services\TripSorter\exceptions
 */
class WriterStringConvertException extends \Exception
{
    
}