<?php

namespace App\services\TripSorter\exceptions;

/**
 * TripSorter class __construct param has invalid type
 * Class ConstructException
 * @package App\services\TripSorter\exceptions
 */
class ConstructException extends \Exception
{

}