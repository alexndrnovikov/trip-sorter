<?php

namespace App\services\TripSorter\exceptions;

/**
 * Route has several boarding cards with same department point
 * Class MultipleDeparturePointsException
 * @package App\services\TripSorter\exceptions
 */
class MultipleDeparturePointsException extends \Exception
{

}