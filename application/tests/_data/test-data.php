<?php

return [
    new class() implements \App\services\TripSorter\interfaces\BoardingCardInterface {
        public function getDeparturePoint() { return  'Gerona Airport';}
        public function getDestinationPoint() { return 'Stockholm';}
        public function getSeat() { return '3A';}
        public function getTransportId() { return 'SK455';}
        public function getGate() { return '45B';}
        public function getTransportType() { return 'flight';}
        public function getBaggageInfo() { return 'Baggage drop at ticket counter 344.';}
        public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
            $this->getDeparturePoint(),
            $this->getTransportType(),
            $this->getTransportId(),
            $this->getDestinationPoint(),
            $this->getGate(),
            $this->getSeat(),
            $this->getBaggageInfo()
        ); }
    },
    new class() implements \App\services\TripSorter\interfaces\BoardingCardInterface {
        public function getDeparturePoint() { return 'Stockholm';}
        public function getDestinationPoint() { return 'New York JFK';}
        public function getSeat() { return '7B';}
        public function getTransportType() { return 'flight';}
        public function getTransportId() { return 'SK22';}
        public function getGate() { return 22;}
        public function getBaggageInfo() { return 'Baggage will we automatically transferred from your last leg.';}
        public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
            $this->getDeparturePoint(),
            $this->getTransportType(),
            $this->getTransportId(),
            $this->getDestinationPoint(),
            $this->getGate(),
            $this->getSeat(),
            $this->getBaggageInfo()
        ); }
    },
    new class() implements \App\services\TripSorter\interfaces\BoardingCardInterface {
        public function getDeparturePoint() { return 'Barcelona';}
        public function getDestinationPoint() { return 'Gerona Airport';}
        public function getSeat() { return 'No seat assignment.';}
        public function getTransportType() { return 'airport bus';}
        public function __toString() { return sprintf('Take the %s from %s to %s. %s.',
            $this->getTransportType(),
            $this->getDeparturePoint(),
            $this->getDestinationPoint(),
            $this->getSeat()
        ); }
    },
    new class() implements \App\services\TripSorter\interfaces\BoardingCardInterface {
        public function getDeparturePoint() { return 'Madrid';}
        public function getDestinationPoint() { return 'Barcelona';}
        public function getSeat() { return '45B';}
        public function getTransportType() { return 'train';}
        public function getTransportId() { return '78A';}
        public function __toString() { return sprintf('Take %s %s from %s to %s. Sit in seat %s.',
            $this->getTransportType(),
            $this->getTransportId(),
            $this->getDeparturePoint(),
            $this->getDestinationPoint(),
            $this->getSeat()
        ); }
    },
];