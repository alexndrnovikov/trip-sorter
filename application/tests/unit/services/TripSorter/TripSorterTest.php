<?php
namespace services\TripSorter;

use App\services\TripSorter\{
    exceptions\ConstructException,
    interfaces\BoardingCardInterface,
    TripSorter
};
use \Codeception\TestCase\Test;

class TripSorterTest extends Test
{
    private $boardCards;

    /**
     * Object construct scenarios
     */
    public function testConstruct()
    {
        $this->expectException(\TypeError::class);
        new TripSorter();

        $this->expectException(ConstructException::class);
        new TripSorter([1,2,3]);

        $this->expectException(ConstructException::class);
        new TripSorter([new \stdClass(), new \stdClass(), new \stdClass()]);

        $sorter = new TripSorter([]);
        $this->assertInstanceOf(TripSorter::class, $sorter);
        
        $this->assertInstanceOf(TripSorter::class, new TripSorter($this->boardCards));
    }

    /**
     * Algorithm result check
     */
    public function testSort()
    {
        $map = (new TripSorter($this->boardCards))->getSortedRouteMap();
        $this->assertSame('Madrid', $map[0]->getDeparturePoint());
        $this->assertSame('New York JFK', $map[count($map)-1]->getDestinationPoint());

    }


    /**
     * Pre-fill boarding cards
     */
    protected function _before()
    {
        $this->boardCards = [
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return  'Gerona Airport';}
                public function getDestinationPoint() { return 'Stockholm';}
                public function getSeat() { return '3A';}
                public function getTransportId() { return 'SK455';}
                public function getGate() { return '45B';}
                public function getTransportType() { return 'flight';}
                public function getBaggageInfo() { return 'Baggage drop at ticket counter 344.';}
                public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
                    $this->getDeparturePoint(),
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDestinationPoint(),
                    $this->getGate(),
                    $this->getSeat(),
                    $this->getBaggageInfo()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Stockholm';}
                public function getDestinationPoint() { return 'New York JFK';}
                public function getSeat() { return '7B';}
                public function getTransportType() { return 'flight';}
                public function getTransportId() { return 'SK22';}
                public function getGate() { return 22;}
                public function getBaggageInfo() { return 'Baggage will we automatically transferred from your last leg.';}
                public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
                    $this->getDeparturePoint(),
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDestinationPoint(),
                    $this->getGate(),
                    $this->getSeat(),
                    $this->getBaggageInfo()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Barcelona';}
                public function getDestinationPoint() { return 'Gerona Airport';}
                public function getSeat() { return 'No seat assignment.';}
                public function getTransportType() { return 'airport bus';}
                public function __toString() { return sprintf('Take the %s from %s to %s. %s.',
                    $this->getTransportType(),
                    $this->getDeparturePoint(),
                    $this->getDestinationPoint(),
                    $this->getSeat()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Madrid';}
                public function getDestinationPoint() { return 'Barcelona';}
                public function getSeat() { return '45B';}
                public function getTransportType() { return 'train';}
                public function getTransportId() { return '78A';}
                public function __toString() { return sprintf('Take %s %s from %s to %s. Sit in seat %s.',
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDeparturePoint(),
                    $this->getDestinationPoint(),
                    $this->getSeat()
                ); }
            },
        ];
    }
}