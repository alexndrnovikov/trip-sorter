<?php
namespace services\TripSorter;

use App\services\TripSorter\{
    exceptions\WriterStringConvertException,
    interfaces\BoardingCardInterface,
    RouteWriter,
    TripSorter
};
use \Codeception\Test\Unit;

class RouteWriterTest extends Unit
{
    public $boardCards;

    /**
     * Construct scenarios check
     */
    public function testConstruct()
    {
        $this->assertInstanceOf(RouteWriter::class, new RouteWriter([1,2,3]));
        $this->assertInstanceOf(RouteWriter::class, new RouteWriter((new TripSorter($this->boardCards))->getSortedRouteMap()));
        $this->expectException(WriterStringConvertException::class);
        $failCode = new RouteWriter([new \stdClass(), new \stdClass()]);
    }

    /**
     * Writing scenarios check 
     */
    public function testWrite()
    {
        $lineEnd = PHP_EOL;
        $this->assertSame("1. Take train 78A from Madrid to Barcelona. Sit in seat 45B.{$lineEnd}2. Take the airport bus from Barcelona to Gerona Airport. No seat assignment..{$lineEnd}3. From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.{$lineEnd}4. From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will we automatically transferred from your last leg.{$lineEnd}5. You have arrived at your final destination.",
            (string)(new RouteWriter((new TripSorter($this->boardCards))->getSortedRouteMap()))
        );

        $this->assertSame("1. 1{$lineEnd}2. 2{$lineEnd}3. 3{$lineEnd}4. You have arrived at your final destination.",
            (string)(new RouteWriter([1,2,3]))
        );
        
    }

    /**
     * Pre-fill boarding cards
     */
    protected function _before()
    {
        $this->boardCards = [
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return  'Gerona Airport';}
                public function getDestinationPoint() { return 'Stockholm';}
                public function getSeat() { return '3A';}
                public function getTransportId() { return 'SK455';}
                public function getGate() { return '45B';}
                public function getTransportType() { return 'flight';}
                public function getBaggageInfo() { return 'Baggage drop at ticket counter 344.';}
                public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
                    $this->getDeparturePoint(),
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDestinationPoint(),
                    $this->getGate(),
                    $this->getSeat(),
                    $this->getBaggageInfo()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Stockholm';}
                public function getDestinationPoint() { return 'New York JFK';}
                public function getSeat() { return '7B';}
                public function getTransportType() { return 'flight';}
                public function getTransportId() { return 'SK22';}
                public function getGate() { return 22;}
                public function getBaggageInfo() { return 'Baggage will we automatically transferred from your last leg.';}
                public function __toString() { return sprintf('From %s, take %s %s to %s. Gate %s, seat %s. %s',
                    $this->getDeparturePoint(),
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDestinationPoint(),
                    $this->getGate(),
                    $this->getSeat(),
                    $this->getBaggageInfo()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Barcelona';}
                public function getDestinationPoint() { return 'Gerona Airport';}
                public function getSeat() { return 'No seat assignment.';}
                public function getTransportType() { return 'airport bus';}
                public function __toString() { return sprintf('Take the %s from %s to %s. %s.',
                    $this->getTransportType(),
                    $this->getDeparturePoint(),
                    $this->getDestinationPoint(),
                    $this->getSeat()
                ); }
            },
            new class() implements BoardingCardInterface {
                public function getDeparturePoint() { return 'Madrid';}
                public function getDestinationPoint() { return 'Barcelona';}
                public function getSeat() { return '45B';}
                public function getTransportType() { return 'train';}
                public function getTransportId() { return '78A';}
                public function __toString() { return sprintf('Take %s %s from %s to %s. Sit in seat %s.',
                    $this->getTransportType(),
                    $this->getTransportId(),
                    $this->getDeparturePoint(),
                    $this->getDestinationPoint(),
                    $this->getSeat()
                ); }
            },
        ];
    }
}