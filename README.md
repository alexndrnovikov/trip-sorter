###Test for PropertyFinder

Code is written in php7.1, so I include vagrant configuration that can handle it.
To install (if vagrant is installed in system) run in terminal in the root of cloned repo:

```
vagrant up
```

To log to vagrant
```
vagrant ssh
```

To navigate to project root
```
cd /var/www/trip-sorter.local
```

To run tests run in terminal
```
codecept run
```

Main idea of realization is that all incoming boarding cards should implement one interface, expected by the sorter service.

Interface expects __toString() implementation, thus new types of transportation and it's wording can be extended outside the service.
